FROM openjdk:8
EXPOSE 8080
ADD build/libs/blog-*.jar ./springboot-k8s-demo.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/springboot-k8s-demo.jar"]