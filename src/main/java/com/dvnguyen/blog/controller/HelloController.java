
package com.dvnguyen.blog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("hello")
public class HelloController {
    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);
    public static List<Double> list = new ArrayList<>();

    @GetMapping("/test")
    public String hello() {
        logger.info("The time now is {}", System.nanoTime());
        return "hello world - commit from dev";
    }

    @RequestMapping("/test2")
    public String hello2() {
        logger.info("HElLO WORLD2223!!!");
        // Make an exception.
        System.out.println("a:" + 10/0);
        return "hello world";
    }

    @RequestMapping("/memory")
    public String memory() {
        logger.info("MEMORY ISSUE!!!");
        for (int i = 0; i < 1000000; i++) {
            list.add(Math.random());
        }
        return "memory test";
    }
}