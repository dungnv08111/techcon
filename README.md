# GitLab CI/CD pipeline with Spring Boot

## About

Blog application with Spring Boot, PostgreSQL and Hibernate using GitLab CI/CD.

## More about

Read the article on Medium: https://dvnguyen.com/setup-a-gitlab-ci-cd-pipeline-using-spring-boot-5c3757f3fec0

git tag -a v0.0.1 -m "test"
git push origin TAG_NAME